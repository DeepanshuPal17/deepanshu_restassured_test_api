import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.equalTo;

import io.restassured.response.Response;
import org.json.simple.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.HashMap;
import java.util.Map;

public class SimpleBookAPITests {
    public static String token;
    public static String name;
    public static String orderId;
    @Test(priority = 0)
    void getStatus() {
        Response response = get("https://simple-books-api.glitch.me/status");
        Assert.assertEquals(response.getStatusCode(),200);
    }

    @Test(priority = 1)
    void postApi(){

        Map<String,String> clientDetails = new HashMap<>();
        String characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        for (int index = 0; index < 10; index++) {
            name += characters.charAt((int) Math.floor(Math.random() * characters.length()));
        }
        clientDetails.put("clientName",name);
        clientDetails.put("clientEmail",name+"@gmail.com");
        System.out.println(clientDetails);
        JSONObject request =new JSONObject(clientDetails);
        System.out.println(request);
        baseURI = "https://simple-books-api.glitch.me";
        token=given().
                header("Content-Type","application/json").
                body(request.toJSONString()).
                when().
                post("/api-clients").
                then().
                statusCode(201).
                extract().
                path("accessToken");
        System.out.println(token);
    }
    @Test(priority = 2)
    void postOrder(){
        baseURI ="https://simple-books-api.glitch.me";
        JSONObject request = new JSONObject();
        request.put("bookId", 1);
        request.put("customerName",name);
        orderId = given()
                .header("Content-Type", "application/json")
                .header("Authorization","Bearer "+token)
                .body(request.toJSONString())
                .when()
                .post("/orders")
                .then()
                .statusCode(201)
                .body("created",equalTo(true))
                .extract()
                .path("orderId");
    }
    @Test(priority = 3)
    void updateOrderName () {
        baseURI ="https://simple-books-api.glitch.me";
        System.out.println(orderId);
        JSONObject request = new JSONObject();
        request.put("customerName","ASDCFV");
        given()
                .header("Content-Type", "application/json")
                .header("Authorization","Bearer "+token)
                .body(request.toJSONString())
                .when()
                .patch("/orders/"+orderId)
                .then()
                .statusCode(204);
    }
    @Test(priority = 4)
    void deleteOrder() {
        baseURI ="https://simple-books-api.glitch.me";
        System.out.println(orderId);
        given()
                .header("Content-Type", "application/json")
                .header("Authorization","Bearer "+token)
                .when()
                .delete("/orders/"+orderId)
                .then()
                .statusCode(204);
    }
}

